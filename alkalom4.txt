
/**
 * Tartalomkezel�k, git
 */
 
https://git-for-windows.github.io/
$ /c/Users/Szum/Documents/Visual\ Studio\ 2013/Projects/LeavesFall/LeavesFall

$ git config --global user.name "username"
$ git config --global user.email "email@email.em"
# Config (tal�n)

$ git init
# Inicializ�l�s : Fel�ll�tja a .git k�nyvt�rat, minden projekt kezdet�n�l ez legyen az els� parancs.
# (ha van m�r .git mappa, nem szabad haszn�lni)

$ git add -A
$ git add <filename>
# �j �s m�dos�tott f�jlok .git-be rak�sa (update)
# -A (�sszes �j/m�dos�tott f�jl)
# <filename> (csak 1 file)

$ git status 
# inf� (mester, branchek, m�g nem addolt fileok)

$ git commit -m "commit komment"
# M�rf�ldk� let�tele (ezek a m�rf�ldk�vek kicsi egys�gek legyenek)
# Kommentek legyenek angolul, felsz�l�t� m�dban (r�vids�g miatt, nem kell folyamatos -ing m�d)
# Tudni lehessen a kommentr�l, mi t�rt�nt abban a commitban
# Pl.: "Add character animations", "Optimize search function", "Exit screen bug fix"

$ git log
$ git log --oneline
# Commitok list�z�sa

$ git push
# Internetre, repositoryj�ba felt�lti

$ git pull
# Internetr�l leszedi

$ git checkout 01e7dba -- visszaallitando.cs #azonos�t� n�h�ny bet�je
$ git checkout 01e7dba # minden f�jl vissza�ll�t�sa
$ git status # megjelent modified-k�nt a visszaallitando.cs
$ git commit -am "undo..." # add-ol�ssal egyben
# Vissza�ll�t�s

/* M�k�d�s kommentek n�lk�l */

	/* Setup */
	$ cd /c/Users/Szum/Documents/Visual\ Studio\ 2013/Projects/LeavesFall/LeavesFall
	$ git init
	$ git remote add origin https://bitbucket.com/szum7/LeavesFall.git

	/* Workflow */
	$ git add -A
	$ git commit -m "Do something"
	$ git push

/* Hasznos */
https://bitbucket.org/
https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html


/**
 * UML - oszt�lydiagram
 */

1. Oszt�ly doboz 
	- Adattagok
		adattagn�v: t�pus (pl.: name: string)
	- Met�dusok
		Met�dusN�v(): visszat�r�si�rt�k (pl.: GetAvgAge(): int vagy MoveLeft() - ez void)
	"+" publikus
	"-" private

2. �r�kl�s
	- Gyerek fel�l mutat a sz�l� fel�

3. Kompoz�ci�
	Egy oszt�ly oszt�ly szint� adattaggal rendelkezik.
	- A birtokolt adattag NEM l�tezik az oszt�lyon k�v�l
		(�ltal�ban itt az oszt�lyon bel�l sz�letik meg(?))
	- A ny�l a birtok ir�ny�b�l mutat a birtokl�j�ba

4. Aggreg�ci�
	Egy oszt�ly oszt�ly szint� adattaggal rendelkezik.
	- A birtokolt adattag l�tezhet az oszt�lyon k�v�l is
		(�ltal�ban konstruktor�ban kapja meg param�terk�nt)
	- A ny�l a birtok ir�ny�b�l mutat a birtokl�j�ba

5. Interface asszoci�ci�
	Az adott oszt�ly megal�s�tja az adott interface-t
	- Fej n�lk�li nyil

+ Konvenci�k
	- kis kezd�bet�
		- v�ltoz�k, adattagok
	- nagy kezd�bet�
		- met�dusok, f�ggv�nyek, oszt�ly szint� tagok (oszt�lynevek, enumok, interfacek)
	- camel casing 
		Pl.: valtozoVagyok, MetodusVagyokJee
	- csupa nagy bet�, elv�laszt�s "_"
		- konstansok : v�ltoz�k, amik nem fognak v�ltozni a program sor�n, konstans �rt�kek. c# const int VALTOZO
			Pl.: MAX, BOARD_COLS
	- Interfaces: IRepulniTudo, IInterfaceVagyokJee
	
+ Plusz konvenci�k
	- "_" kezd�d�
		- bels� priv�t adattagokn�l, melyek nem hagyj�k el az oszt�lyt
			(�n csak akkor szoktam n�ha haszn�lni, ha pl egy t�mb adattaggal index adattagot akarok haszn�lni
			Pl.: private int _i, _j
		
		
/**
 * Plusz
 */
// 111 program
int a = 111;
Console.WriteLine("Hello world, {0}, {1}", a, a);
Console.ReadLine();





