﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AronféleEtterem
{
    class VendegCsoport
    {
        string nev;
        int darabszam;
        AsztalType igeny;
        public VendegCsoport(string nev,int darabszam,AsztalType igeny)
        {
            this.nev = nev;
            this.darabszam = darabszam;
            this.igeny = igeny;
        }
        public override string ToString()
        {
            return nev + " (" + darabszam + "), " + igeny;
        }
        public VendegCsoport()
        {
            nev = Constants.RandomCharChain(10);
            darabszam = Constants.Rnd.Next(0, 10);
            igeny = (AsztalType)Constants.Rnd.Next(0,3);
          //  nev = C1.RandomCharChain();
        //    darabszam = C1.Random(0, 10);
          //  (AsztalType)
           // (Asztalok.AsztalType)C1.Random(0, 3);
          //  (A1)C1.Random(0, 3);
           // Enum.GetValues(typeof(Asztalok.AsztalType));

        }
    }
}
