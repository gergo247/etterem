﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AronféleEtterem
{
    enum AsztalType { Sima, Közép, Luxus }
    class Constants
    {
        
        public static Random Rnd = new Random();

       public static string RandomCharChain(int hossz)
        {
            string s = "";
            string word = "QWERTZUIOPŐÚŰÁÉLKJHGFDSAÍYXCVBNM";
            for (int i = 0; i < hossz; i++)
            {
                s += word.Substring(Rnd.Next(0, word.Length), 1);
            }

            return s;
        }

    }
}
